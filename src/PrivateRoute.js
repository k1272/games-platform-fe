import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { AuthContext } from "./Auth";

function PrivateRoute({ children }){
  const {currentUser} = useContext(AuthContext);
  console.log(currentUser);
  if (!currentUser) {
    return <Navigate to="/login" />;
  }
  return children;
};


export default PrivateRoute



//   return (
//     <Routes>
//     <Route
//       {...rest}
//       render={routeProps =>
//         !!currentUser ? (
//           <RouteComponent {...routeProps} />
//         ) : (
//           <Navigate to={"/login"} />
//         )
//       }
//     />
//     </Routes>
//   );