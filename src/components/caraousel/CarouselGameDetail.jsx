import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    CarouselControl,
    Carousel,
    CarouselItem,
    CarouselIndicators,
} from 'reactstrap';

import badminton from '../../assets/images/badminton-game.jpg';
import rpsGame from '../../assets/images/rps-game.jpg';
import squidGame from '../../assets/images/squid-game.jpg';
import { useNavigate } from 'react-router-dom';

function CarouselGameDetail() {
    // State for Active index
    const [activeIndex, setActiveIndex] = React.useState(0);

    // State for Animation
    const [animating, setAnimating] = React.useState(false);

    // Sample items for Carousel
    const items = [
        {
            // caption: 'Sample Caption One',
            url: badminton,
            gameName: 'badminton'
        },
        {
            // caption: 'Sample Caption Two',
            url: rpsGame,
            gameName: 'rock-paper-scissors'
        },
        {
            // caption: 'Sample Caption Two',
            url: squidGame,
            gameName: 'squid-game'
        }
    ];

    // Items array length
    const itemLength = items.length - 1

    // Previous button for Carousel
    const previousButton = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ?
            itemLength : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    // Next button for Carousel
    const nextButton = () => {
        if (animating) return;
        const nextIndex = activeIndex === itemLength ?
            0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const navigate = useNavigate();
    // Carousel Item Data
    const carouselItemData = items.map((item) => {
        return (
            <CarouselItem
                key={item.url}
                onExited={() => setAnimating(false)}
                onExiting={() => setAnimating(true)}
            >
                <img src={item.url} alt={item.gameName} style={{width: 340, height: 265}} onClick={() => { navigate(`/game/detail/${item.gameName}`, { state: item })}} />
            </CarouselItem>
        );
    });

    return (
        <div style={{
            display: 'inline-block', width: 340,height: 265, cursor: 'pointer',
        }}>
            <Carousel previous={previousButton} next={nextButton}
                activeIndex={activeIndex}>
                <CarouselIndicators items={items}
                    activeIndex={activeIndex}
                    onClickHandler={(newIndex) => {
                        if (animating) return;
                        setActiveIndex(newIndex);
                    }} />
                {carouselItemData}
                <CarouselControl directionText="Prev"
                    direction="prev" onClickHandler={previousButton} />
                <CarouselControl directionText="Next"
                    direction="next" onClickHandler={nextButton} />
            </Carousel>
        </div >
    );
}

export default CarouselGameDetail;