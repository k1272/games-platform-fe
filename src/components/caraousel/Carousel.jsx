import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
	CarouselControl,
	Carousel,
	CarouselItem,
	CarouselIndicators,
} from 'reactstrap';
import { useNavigate } from 'react-router-dom';

import catur from '../../assets/images/catur.jpg'
import shark from '../../assets/images/shark.jpg'
import sliterIo from '../../assets/images/slitherio.jpg'
import racing from '../../assets/images/racing.jpg'

function Carousels() {

	// State for Active index
	const [activeIndex, setActiveIndex] = React.useState(0);

	// State for Animation
	const [animating, setAnimating] = React.useState(false);

	// Sample items for Carousel
	const items = [
		{
			// caption: 'Sample Caption One',
			url: catur,
			gameName: 'chase game'
		},
		{
			// caption: 'Sample Caption One',
			url: shark,
			gameName: 'shark game'
		},
		{
			// caption: 'Sample Caption Two',
			url: sliterIo,
			gameName: 'slither io game'
		},
		{
			// caption: 'Sample Caption Two',
			url: racing,
			gameName: 'racing game '
		}
	];
	const navigate = useNavigate();

	// Items array length
	const itemLength = items.length - 1

	// Previous button for Carousel
	const previousButton = () => {
		if (animating) return;
		const nextIndex = activeIndex === 0 ?
			itemLength : activeIndex - 1;
		setActiveIndex(nextIndex);
	}

	// Next button for Carousel
	const nextButton = () => {
		if (animating) return;
		const nextIndex = activeIndex === itemLength ?
			0 : activeIndex + 1;
		setActiveIndex(nextIndex);
	}

	// Carousel Item Data
	const carouselItemData = items.map((item) => {
		return (
			<CarouselItem
				key={item.url}
				onExited={() => setAnimating(false)}
				onExiting={() => setAnimating(true)}
			>
				<img src={item.url} 
					alt={item.gameName} 
					style={{width: 650, height: 350}} 
					className="shadow-sm"
					onClick={() => { navigate(`/game/detail/${items.gameName}`, { state: item })}}
				/>
			</CarouselItem>
		);
	});

	return (
		<div  className="mt-md-2 mb-md-5 " style={{
			display: 'inline-block', width: 650, marginLeft: 200, cursor: 'pointer',
		}}>
			<Carousel previous={previousButton} next={nextButton}
				activeIndex={activeIndex}>
				<CarouselIndicators items={items}
					activeIndex={activeIndex}
					onClickHandler={(newIndex) => {
						if (animating) return;
						setActiveIndex(newIndex);
					}} />
				{carouselItemData}
				<CarouselControl directionText="Prev"
					direction="prev" onClickHandler={previousButton} />
				<CarouselControl directionText="Next"
					direction="next" onClickHandler={nextButton} />
			</Carousel>
		</div >
	);
}

export default Carousels;
