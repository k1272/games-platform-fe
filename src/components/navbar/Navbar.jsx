import React, { useState } from "react";
import { Link } from "react-router-dom";
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem} from 'reactstrap'

const Navbars = () => {
    const [open, setOpen] = useState({ isOpen: false});

    const toggleCollapse = () => {

        setOpen({isOpen: !open.isOpen});
    };

    return (
        <Navbar light expand="lg" className="shadow-sm p-3 mb-4 bg-body" >
            <NavbarBrand href="/" className="ms-lg-5 ps-lg-3">Game</NavbarBrand>
            <NavbarToggler 
                onClick={toggleCollapse}
                aria-controls="collapse"
                aria-expanded={open.isOpen}
            />
            <Collapse isOpen={open.isOpen} navbar>
                <Nav id="collapse" className="mr-auto text-uppercase" navbar >
                    <NavItem className="px-2 ms-lg-5">
                        <Link className="nav-link" to="/home">Home</Link>
                    </NavItem>
                    <NavItem className="px-2">
                        <Link className="nav-link" to="/games">Game</Link>
                    </NavItem>
                    <NavItem className="px-2">
                        <Link className="nav-link" to="#">About</Link>
                    </NavItem>
                </Nav>
                <Nav className="ms-auto text-uppercase me-5 pe-3" navbar>
                    <NavItem className="px-2">
                        <Link className="nav-link" to="/login">Login</Link>
                    </NavItem>
                    <NavItem className="px-2">
                        <Link className="nav-link" to="/register">Signup</Link>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    )
};

export default Navbars;