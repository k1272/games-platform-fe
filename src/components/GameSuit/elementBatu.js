import "../../assets/styles/GameStyle.css";
function Batu(games) {
    return (
        <div className={games.className}>
            <div>
                <img className="batu" onClick={games.onClick} />
            </div>
        </div>

    )
}
export default Batu;