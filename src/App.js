import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import LandingPage from './pages/LandingPage';
import Home from './pages/Home';
import GameList from './pages/GameList';
import GameDetail from './pages/GameDetail';
import RPSGame from './pages/RPSGame';
import Login from './pages/Login';
import Register from './pages/Register';
import Profile from './pages/Profile';
import NotFound from './pages/NotFound';


import { AuthProvider } from "./Auth";
import PrivateRoute from "./PrivateRoute";

function App() {
  return (
    <AuthProvider>
      <Router>
        <Routes>
            <Route path='/' element={<LandingPage/>} />
            <Route path='/home' element={<Home/>} />
            <Route path='/games' element={<PrivateRoute><><GameList/></></PrivateRoute>} />
            <Route path='/game/detail/:name' element={<GameDetail/>} />
            <Route path='/games/rps' element={<RPSGame/>} />
            <Route path='/login' element={<Login/>} />
            <Route path='/register' element={<Register/>} />
            <Route path='/profile' element={<Profile/>} />
            <Route path= '*' element={<NotFound/>} />
        </Routes>
      </Router>
    </AuthProvider>
  );
}

export default App;