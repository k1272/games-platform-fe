import Navbar from '../components/navbar/Navbar';
import '../assets/styles/Login.css'
import axios from 'axios';
import {useState} from 'react';
import {useNavigate} from 'react-router-dom';

const Home = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    const Auth = async(e) => {
        e.preventDefault();
        try{
            await axios.post('http://localhost:5000/login', {username, password})
            .then(result => {
                localStorage.setItem("token", result.data.token)
            })
            // console.log(e)
            navigate('/home');
        } catch(error){
            if(error.response){
                console.log(error.response);
            }
        }
    }

    // di login ngehit API
    // Tangkap email password dari form, terus salurin ke API
    // Setelah itu API dapet respon token
    // Tokennya nanti ditaro di localstorage (pake localStorage.setItem(Key, Value Token))
    
    // Login, dapet token, token simpen ke localstorage, setelah dapet diredirect ke halaman utama

    return (
        <>
        <Navbar />
        <div class="Login">
            <div class="container-fluid ps-md-0">
                <div class="row g-0">
                    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
                    <div class="col-md-8 col-lg-6">
                    <div class="login d-flex align-items-center py-5">
                        <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-lg-8 mx-auto">
                            <h3 class="login-heading mb-4">Welcome back</h3>

                            
                            <form onSubmit={Auth}>
                                <div class="form-floating mb-3">
                                <input type="text" class="form-control" id="username" placeholder="" value={username} onChange={(e) => setUsername(e.target.value)}/>
                                <label for="floatingInput">Username</label>
                                </div>
                                <div class="form-floating mb-3">
                                <input type="password" class="form-control" id="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                                <label for="floatingPassword">Password</label>
                                </div>

                                <div class="form-check mb-3">
                                <input class="form-check-input" type="checkbox" value="" id="rememberPasswordCheck"/>
                                <label class="form-check-label" for="rememberPasswordCheck">
                                    Remember password
                                </label>
                                </div>

                                <div class="d-grid">
                                <button class="btn btn-lg btn-primary btn-login text-uppercase fw-bold mb-2" type="submit">Sign in</button>
                                <div class="text-center">
                                    <a class="small" href="#">Forgot password?</a>
                                </div>
                                </div>

                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Home;