import { Container } from "reactstrap";
import Navbar from '../components/navbar/Navbar';
import '../assets/styles/Login.css'
import axios from "axios";

const Home = () => {

    let token = localStorage.getItem("token")

    let playerFullname = ''
    let playerAddress = ''
    let playerCity = ''
    let playerPhoneNumber = ''

    axios.get('http://localhost:5000/profile', {headers : {
        'Authorization' : `${token}`
      }})
         .then(function(response){
             playerFullname = response.data.full_name
             playerAddress = response.data.address
             playerCity = response.data.city
             playerPhoneNumber = response.data.phone_number
         })
         .catch(function(error){
             console.log(error)
         })
         .then(function(){})
    return (
        <div>
            <Navbar />
            <Container >
            <div class="container rounded bg-white mt-5 mb-5">
                <div class="row">
                    <div class="col-md-3 border-right">
                        <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                            <img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg" alt="hero-
                            " />
                            <span class="font-weight-bold">Edogaru</span>
                            <span class="text-black-50">laodeimansyahrin@yahoo.com</span>
                            <span></span>
                        </div>
                    </div>
                    
                    <div class="col-md-5 border-right">
                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="text-right">Profile Settings</h4>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <label class="labels">Fullname</label>
                                    <input type="text" class="form-control" id="Fullname"placeholder={playerFullname} value=""/>
                                </div>

                                <div class="col-md-6">
                                    <label class="labels">Address</label>
                                    <input type="text" class="form-control" id="Address" value="" placeholder={playerAddress}/>
                                </div>
                            </div>
                            <div class="row mt-3">
                            <form method="get" action="/profile">
                                <div class="col-md-12">
                                    <label class="labels">City</label>
                                    <input type="text" class="form-control" id="City" placeholder={playerCity} value=""/>
                                </div>
                            </form>
                            <form method="post" action="/profile">
                                <div class="col-md-12">
                                    <label class="labels">Phone Number</label>
                                    <input type="text" class="form-control" id="Phone Number"placeholder={playerPhoneNumber} value=""/>
                                </div>
                                </form>
                            </div>
                            <div class="mt-5 text-center">
                                <button class="btn btn-primary profile-button" type="button">Save Profile</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 py-5">
                            <br></br>
                        </div>
                    </div>
                </div>
            </div>
            </Container>
        </div>
    )
}

export default Home;
