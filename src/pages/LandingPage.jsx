import { Container, Row } from "reactstrap";
import { Link } from "react-router-dom";
import Navbar from '../components/navbar/Navbar';
import game from '../assets/images/gamers.jpg'

const Home = () => {
    return (
        <div>
            <Navbar />
            <Container >
                <Row className="shadow p-md-3 mb-5 bg-body rounded m-4">
                    <div className="col-md-12 col-lg-6 order-lg-2 d-flex p-md-4 justify-content-center text-center">
                        <img src={game} alt="game-console"  className=" img-fluid" />
                    </div>
                    <div className="p-lg-5 py-3 col-md-12 col-lg-6 order-md-1">
                        <p className="form-text">want to play games?</p>
                        <h1 className="fs-1 fw-bold">Let’s Play!</h1>
                        <p style={{color: '#5876a3'}}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Possimus modi voluptatem doloremque, laborum reprehenderit quod tempore veritatis corporis repellendus asperiores distinctio velit ipsa nesciunt repellat odio explicabo recusandae optio? Quo perferendis nemo repudiandae id delectus ab.</p>
                        <Link to="/login" className="btn btn-primary px-3 mt-lg-3">Get Started</Link>
                    </div>
                </Row>
            </Container>
        </div>
    )
}

export default Home;