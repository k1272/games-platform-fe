import React from 'react';

const NotFound = () => {
    return (
        <div className="bg-light d-flex justify-content-center align-items-center" style={{height: '100vh'}}>
            <div>
                <h1>404 Not Found</h1>
                <p>The page that you're looking for doesn't exist!</p>
                <a href="/" class="btn btn-sm btn-primary px-4 py-1">Go to Home</a>
            </div>
        </div>
    )
}

export default NotFound;
