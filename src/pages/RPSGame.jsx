import React from 'react'
import '../assets/styles/RPS.css'
import { Col, Container, Row } from 'reactstrap'
import pre from '../assets/images/pre.png'
import logoGame from '../assets/images/logo-game.png'
// import bgGame from '../assets/bg-games.png'
import batu from '../assets/images/batu.png'
import gunting from '../assets/images/gunting.png'
import kertas from '../assets/images/kertas.png'
import refresh from '../assets/images/refresh.png'
import { Suit } from "../components/GameSuit/suitt"
import Batu from "../components/GameSuit/elementBatu"
import Gunting from "../components/GameSuit/elementGunting"
import Kertas from "../components/GameSuit/elementKertas"
import { useState, useEffect } from "react"
import { Link } from "react-router-dom";
import axios from 'axios'

const GameSuit = () => {
  // let tangan = '';

    // const Game = async(e) => {
    //     tangan = e.target.value;
    // }

    // try{
    //     await axios.post('http://localhost:5000/game', {tangan});
    // }
    // catch(error){
    //     console.log(error);
    // }

    let token = localStorage.getItem("token")

    const sendResult = async() => {
      try{
        await axios.post('http://localhost:5000/game/rps', {result}, {headers : {
          'Authorization' : `${token}`
        }})
      }
      catch(error){
        console.log(error);
      }
    }


    // axios.get('http://localhost:5000/game/rps')
    //      .then(function(response){
    //          console.log(response);
    //      })
    //      .catch(function(error){
    //          console.log(error);
    //      })
    //      .then(function(){

    //      })

    let game = new Suit()
    const [vs, Setvs] = useState("VS")
    const [BatuKomputer, SetBatuKomputer] = useState(null)
    const [guntingKomputer, SetGuntingKomputer] = useState(null)
    const [KertasKomputer, SetKertasKomputer] = useState(null)
    const [stop, ] = useState(false)
    let result = ('');
    
    let count = {}
    useEffect(() => {
    
      setTimeout(() => {
        SetKertasKomputer(null)
        SetBatuKomputer(null)
        SetGuntingKomputer(null)
      }, 5000);
    })

    const clickGunting = () => {
      let comp = game.computer()
      if (stop === false) {
        if (comp === "batu") {
          SetBatuKomputer("margin-grey")
          Setvs("Player-Win")
          result = 'PlayerWin';
          sendResult();
        } else if (comp === "kertas") {
          SetKertasKomputer("margin-grey")
          Setvs("Com-Win")
          result = 'ComWin';
          sendResult();
        } else if (comp === "gunting") {
          SetGuntingKomputer("margin-grey")
          Setvs("Draw");
          result = 'Draw';
          sendResult();
        } 
        
      }
    }
  
    const clickBatu = () => {
      let comp = game.computer()
      if (stop === false) {
        if (comp === "batu") {
          SetBatuKomputer("margin-grey")
          Setvs("Draw")
          result = 'Draw';
          sendResult();
        } else if (comp === "kertas") {
          Setvs("Player-Win")
          SetKertasKomputer("margin-grey")
          result = 'PlayerWin';
          sendResult();
        } else if (comp === "gunting") {
          Setvs("Com-Win")
          SetGuntingKomputer("margin-grey")
          result = 'ComWin';
          sendResult();
        } 
        
      }
    };
    
    const clickKertas = () => {
      let comp = game.computer()
      if (stop === false) {
        if (comp === "batu") {
          SetBatuKomputer("margin-grey")
          Setvs(`Com-Win`)
          result = 'ComWin';
          sendResult();
        } else if (comp === "kertas") {
          SetKertasKomputer("margin-grey")
          Setvs("Draw")
          result = 'Draw';
          sendResult();
        } else if (comp === "gunting") {
          SetGuntingKomputer("margin-grey")
          Setvs("Player-Win")
          result = 'PlayerWin';
          sendResult();
        } 
  
      }
    };
    const refreshButton = () => {
      Setvs("Versus")
      // window.location.reload()
      SetKertasKomputer(null)
      SetBatuKomputer(null)
      SetGuntingKomputer(null)
    };

    
    return (
        <div className="bg-game">
            <Row className=" pt-md-4 ms-md-5 d-flex align-items-center">
                <Col className="col-1">
                  <button type="button" className="bg-transparent border-0 mt-auto" id="choice-style">
                    <Link to="/games" className="menu-link">
                      <img src={pre} alt="back" width={40} className={"margin"} />
                    </Link>
                  </button>
                </Col>
                <Col className="col-1"><img src={logoGame} alt="back" width={72} /></Col>
                <Col className="text-warning"><h4>ROCK PAPER SCISSORS</h4></Col>
            </Row>
            <Row className="d-flex justify-content-evenly mt-4">
                <Col className='col-md-3 d-flex flex-column' style={{height: '70vh'}}>
                    <h2 className="text-center mb-md-4">PLAYER</h2>
                    <button type="button" className="bg-transparent border-0" id="choice-style">
                        <img src={batu} alt="rock" class="rock" width={90} 
                        Batu onClick={() => clickBatu()} className={"margin"} />

                    </button>
                    <button type="button" className="bg-transparent border-0 mt-auto" id="choice-style">
                        <img src={kertas} alt="paper" class="paper" width={90} 
                        Gunting onClick={() => clickGunting()} className={"margin"}
                      />
                    </button>
                    <button type="button" className="bg-transparent border-0 mt-auto" id="choice-style">
                        <img src={gunting} alt="scissor" class="scissors" width={90} 
                        Kertas onClick={() => clickKertas()} className={"margin"} />
                    </button>
                </Col>
                <Col className='col-md-3 d-flex flex-column'>
                <div>
                  <div className="Hasil">
                    {vs}
                  </div>
                </div>
                    <button type="button" className="bg-transparent border-0 mt-auto" onClick={() => refreshButton()} id="choice-style">
                        <img src={refresh} alt="refresh" className="" width={35} />
                    </button>
                </Col>
                <Col className='col-md-3 d-flex flex-column' style={{height: '70vh'}}>
                    <h2 className="text-center mb-md-4">COM</h2>
                    <button type="button" className="bg-transparent border-0" id="choice-style">
                        <img src={batu} alt="rock" class="rock" width={90} 
                        Batu className={BatuKomputer ? BatuKomputer : "margin"} />
                    </button>
                    <button type="button" className="bg-transparent border-0 mt-auto" id="choice-style">
                        <img src={kertas} alt="paper" class="paper" width={90} 
                        Gunting className={guntingKomputer ? guntingKomputer : "margin"}
                      />
                    </button>
                    <button type="button" className="bg-transparent border-0 mt-auto" id="choice-style">
                        <img src={gunting} alt="scissor" class="scissors" width={90} 
                        Kertas className={KertasKomputer ? KertasKomputer : "margin"}
                      />
                    </button>
                </Col>
            </Row>
        </div>
    )
}

export default GameSuit












