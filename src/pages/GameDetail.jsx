import React, { useState } from 'react';
import { useLocation } from "react-router-dom";
import { Container, Row, Col, Card, CardImg } from 'reactstrap';
import Navbars from '../components/navbar/Navbar';
import { Link, useNavigate } from 'react-router-dom';
import '../assets/styles/GameList.css';

import badminton from '../assets/images/badminton-game.jpg';
import catur from '../assets/images/catur.jpg';
import footbalGame from '../assets/images/football-game.jpg';
import panahGame from '../assets/images/panah-game.jpg';
import CarouselGameDetail from '../components/caraousel/CarouselGameDetail';

const GameDetail = () => {
    const {state} = useLocation();
    const { gameName, url } = state;
    console.log(state);

    const [game, setGame] = useState([
        { gameName: 'badminton', url: badminton },
        { gameName: 'chess', url: catur },
        { gameName: 'football', url: footbalGame },
        { gameName: 'archery', url: panahGame },
    ]);

    const navigate = useNavigate();

    return (
        <>
            <Navbars />
            <Container>
                <div className="d-flex flex-row">
                    <div className="d-inline-block shadow me-md-5">
                        <img src={url} alt={gameName} className="" width='400' height='270' />
                        <div className="me-lg-5 pe-lg-5 text-capitalize align-self-center ms-lg-5 d-inline-block">
                            <h5 className=''>{gameName}</h5>
                            <Link to="/games/rps" className="btn btn-primary px-4 ">Start Game</Link>
                        </div>
                    </div>
                    <div>
                        <CarouselGameDetail />
                    </div>
                </div>
                <div className="my-lg-4 shadow-sm p-md-4">
                    <h5 className="text-capitalize">About {gameName}</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi harum amet, soluta nostrum officiis laborum, labore debitis, facilis aliquid voluptatem tempore! Harum tenetur vitae molestiae veniam. Quo accusantium perferendis illo amet dolorum iure, unde voluptatibus voluptatem distinctio iste iusto nobis doloribus? Officiis ad ab doloremque dolore minus molestiae eveniet delectus incidunt non dolor, dolorem animi neque obcaecati expedita voluptatum, modi corporis vitae! Quisquam minus culpa quia, animi eligendi totam deserunt perferendis fugit ad eveniet, facere explicabo, mollitia id voluptatem. Ullam dolorem eius blanditiis natus officia! Veniam voluptatum quisquam officiis error vel, similique officia a neque rem praesentium, magni quis mollitia.</p>
                    <h5 className="text-capitalize">Fitur</h5>
                    <ul>
                        <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Hic, voluptate.</li>
                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                        <li>Lorem ipsum dolor sit amet.</li>
                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur!</li>
                    </ul>

                    <h5 className="text-capitalize">how to play ?</h5>
                    <ol>
                        <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsam quia officiis commodi aspernatur nihil ipsum.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                        <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, fuga.</li>
                        <li>Lorem ipsum dolor sit amet.</li>
                        <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quisquam veniam beatae fugiat. Consectetur?</li>
                    </ol>

                    <h5 className="text-capitalize">Rules</h5>
                    <ol>
                        <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsam quia officiis commodi aspernatur nihil ipsum.</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                        <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, fuga.</li>
                    </ol>
                </div>
                <Row className="mb-md-5">
                { game.map(game => (
                    <Col className="col-md-3" key={game.gameName} onClick={() => { navigate(`/game/detail/${game.gameName}`, { state: game })}} > 
                        <Card className="card-style">
                            <CardImg
                                alt={game.gameName}
                                src={game.url}
                                top
                                width="200px"
                                height="180px"
                            />
                        </Card>
                    </Col>
                ))}
                </Row>
            </Container>
        </>
    )
}

export default GameDetail
