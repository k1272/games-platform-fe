import React, { useState } from 'react';
import Navbars from '../components/navbar/Navbar';

import badminton from '../assets/images/badminton-game.jpg';
import catur from '../assets/images/catur.jpg';
import footbalGame from '../assets/images/football-game.jpg';
import panahGame from '../assets/images/panah-game.jpg';
import pokerGame from '../assets/images/poker-game.jpg';
import rpsGame from '../assets/images/rps-game.jpg';
import squidGame from '../assets/images/squid-game.jpg';
import racing from '../assets/images/racing.jpg';
import shark from '../assets/images/shark.jpg';
import slitherio from '../assets/images/slitherio.jpg';
import ticTacToe from '../assets/images/tic-tac-toe.png';
import { Row, Col, Card, CardImg, CardBody, CardTitle, Container } from 'reactstrap';
import { useNavigate } from 'react-router';

import '../assets/styles/GameList.css';

const GameList = () => {
    // data game 
    const [gameRow1, setGameRow1] = useState([
        { gameName: 'badminton', url: badminton },
        { gameName: 'chess', url: catur },
        { gameName: 'football', url: footbalGame },
        { gameName: 'archery', url: panahGame },
    ]);

    const [gameRow2, setGameRow2] = useState([
        { gameName: 'poker', url: pokerGame },
        { gameName: 'tic-Tac-Toe', url: ticTacToe},
        { gameName: 'racing', url: racing },
        { gameName: 'rock-paper-scissors', url: rpsGame },
    ]);

    const [gameRow3, setGameRow3] = useState([
        { gameName: 'shark', url: shark },
        { gameName: 'slither.io', url: slitherio },
        { gameName: 'squid-game', url: squidGame },
    ]);

    const navigate = useNavigate();

    return (
        <>
            <Navbars />
            <Container>
                <Row className='mb-md-4'>
                    { gameRow1.map((game, i) => (
                        <Col className="col-md-3" key={game.gameName} > 
                            <Card className="card-style" onClick={() => { navigate(`/game/detail/${game.gameName}`, { state: game })}}>
                                <CardImg
                                alt={game.gameName}
                                src={game.url}
                                top
                                width="200px"
                                height="180px"
                                />
                                <CardBody>
                                    <CardTitle tag="h6" className="text-capitalize text-center">
                                    {game.gameName}
                                    </CardTitle>
                                </CardBody>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Row className='mb-md-4'>
                    <Col className="col-lg-6">
                        <div className='d-flex flex-row mb-md-4'>
                            <Card style={{ width: 262}} className="me-md-4 card-style" onClick={() => { navigate(`/game/detail/${gameRow2[0].gameName}`, { state: gameRow2[0] })}}>
                                <CardImg
                                    alt={gameRow2[0].gameName}
                                    src={gameRow2[0].url}
                                    top
                                    width="200px"
                                    height="180px"
                                />
                                <CardBody>
                                    <CardTitle tag="h6" className="text-capitalize text-center">
                                        {gameRow2[0].gameName}
                                    </CardTitle>
                                </CardBody>
                            </Card>
                            <Card className="card-style" style={{ width: 262}} onClick={() => { navigate(`/game/detail/${gameRow2[1].gameName}`, { state: gameRow2[1] })}}>
                                <CardImg
                                    alt={gameRow2[1].gameName}
                                    src={gameRow2[1].url}
                                    top
                                    width="200px"
                                    height="180px"
                                />
                                <CardBody>
                                    <CardTitle tag="h6" className="text-capitalize text-center">
                                        {gameRow2[1].gameName}
                                    </CardTitle>
                                </CardBody>
                            </Card>
                        </div>
                        <Card className="card-style" style={{ width: 545}} onClick={() => { navigate(`/game/detail/${gameRow2[2].gameName}`, { state: gameRow2[2] })}}>
                            <CardImg
                                alt={gameRow2[2].gameName}
                                src={gameRow2[2].url}
                                top
                                // width="400px"
                                height="180px"
                            />
                            <CardBody>
                                <CardTitle tag="h6" className="text-capitalize text-center">
                                    {gameRow2[2].gameName}
                                </CardTitle>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col className="col-lg-6" >
                        <Card className="card-style" onClick={() => { navigate(`/game/detail/${gameRow2[3].gameName}`, { state: gameRow2[3] })}}>
                            <CardImg
                                alt={gameRow2[3].gameName}
                                src={gameRow2[3].url}
                                top
                                width="200px"
                                height="445"
                            />
                            <CardBody>
                                <CardTitle tag="h6" className="text-capitalize text-center">
                                    {gameRow2[3].gameName}
                                </CardTitle>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row className="mb-md-5">
                    { gameRow3.map(game => (
                        <Col className="col-md-4" key={game.gameName}> 
                            <Card onClick={() => { navigate(`/game/detail/${game.gameName}`, { state: game })}} className="card-style">
                                <CardImg
                                alt={game.gameName}
                                src={game.url}
                                top
                                width="200px"
                                height="180px"
                                />
                                <CardBody>
                                    <CardTitle tag="h6" className="text-capitalize text-center">
                                    {game.gameName}
                                    </CardTitle>
                                </CardBody>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </Container>
        </>
    )
}

export default GameList;
