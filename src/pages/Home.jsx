import React from 'react';
import Navbars from '../components/navbar/Navbar';
import Carousel from '../components/caraousel/Carousel';
import { Container, Row} from "reactstrap";
import { Link } from "react-router-dom";
import rpsGame from '../assets/images/rps-game.jpg'
import popular from '../assets/images/popularity.png'
import fire from '../assets/images/fire.png'
import axios from 'axios';

const Home = () => {

    // Koding yang di bawah cuma buat ngetes nyambung gak waktu 2-2nya udah di deploy
    // Ternyata nyambung, jadi nasib kodingan di bawah ini aku serahkan pada kalian
    // axios.get('https://test-backend-fsw13kelompok2.herokuapp.com/register')
    // .then(function(response){
    //     console.log(response)
    // });

    axios.get('http://localhost:5000/home')
         .then(function(response){
             console.log(response);
         })
         .catch(function(response){
             console.log(response);
         })
         .then(function(){

         });

    return (
        <div>
            <Navbars />
            <Container >
                <p><img src={popular} className="me-lg-2" alt="popular" width='23px' /> popular game</p>
                <Row className="shadow-sm p-md-3 mb-5 bg-body rounded m-4">
                    <div className="col-md-12 col-lg-3 d-flex p-md-4 justify-content-center text-center ">
                        <img src={rpsGame} alt="game-console"  className=" img-fluid rounded" />
                    </div>
                    <div className="p-lg-2 py-3 col-md-12 col-lg-6">
                        <h1 className="fs-1 fw-bold">Rock Paper Scissors</h1>
                        <p className="pt-lg-2" style={{color: '#5876a3'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <Link to="/login" className="btn btn-primary px-4 ">Start Game</Link>
                    </div>
                </Row>
                <p><img src={fire} className="me-lg-2" alt="popular" /> games recommendations for you <Link to="/games" className="text-decoration-none">more game</Link></p>
                <Carousel />
            </Container>
        </div>
    )
}

export default Home;
