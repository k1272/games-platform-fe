import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios';
import {useNavigate} from 'react-router-dom';
import '../App.css'

export default function SignUpPage() {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const navigate = useNavigate();

    const Register = async(e) => {
        e.preventDefault();
        try{
            await axios.post('http://localhost:5000/register', {username, email, password})
            navigate('/login');
        } catch(error){
            if(error.response){
                console.log(error.response);
            }
        }
    }

    return (
        <div className="text-center m-5-auto">
            <h2>SIgn up</h2>
            <h5>Create your account</h5>
            <form onSubmit={Register}>
                <p>
                    <label>Username</label><br/>
                    <input type="text" class="form-control" id="username" placeholder="" value={username} onChange={(e) => setUsername(e.target.value)}/>
                </p>
                <p>
                    <label>Email address</label><br/>
                    <input type="email" class="form-control" id="email" placeholder="" value={email} onChange={(e) => setEmail(e.target.value)}/>
                </p>
                <p>
                    <label>Password</label><br/>
                    <input type="password" class="form-control" id="password" placeholder="" value={password} onChange={(e) => setPassword(e.target.value)}  />
                </p>
                <p>
                    <input type="checkbox" name="checkbox" id="checkbox" required /> <span>I agree all statements in <a href="https://google.com" target="_blank" rel="noopener noreferrer">terms of service</a></span>.
                </p>
                <p>
                    <button id="sub_btn" type="submit">Register</button>
                </p>
            </form>
            <footer>
                <p><Link to="/">Back to Homepage</Link>.</p>
            </footer>
        </div>
    )

}
